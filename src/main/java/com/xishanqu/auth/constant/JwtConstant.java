package com.xishanqu.auth.constant;

/**
 * @Author BaoNing 2019/4/1
 */
public class JwtConstant {

    /**
     *  5天(以毫秒ms计)
      */
    public static final long EXPIRATION_TIME = 432_000_000;

    /**
     * JWT密码
     */
    public static final String SECRET = "XISHANQU";

    /**
     * Token前缀
     */
    public static final String TOKEN_PREFIX = "Bearer";

    /**
     * 存放Token的Header Key
     */
    public static final String HEADER_STRING = "Authorization";
}
