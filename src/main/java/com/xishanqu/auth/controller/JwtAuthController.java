package com.xishanqu.auth.controller;

import com.xishanqu.auth.entity.User;
import com.xishanqu.auth.service.AuthService;
import com.xishanqu.auth.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.AuthenticationException;

/**
 * @Author BaoNing 2019/4/1
 */
@RestController
@RequestMapping("/api/v1/auth")
public class JwtAuthController {

    @Autowired
    private AuthService authService;

    /**
     * 注册
     * @param user
     * @return
     * @throws AuthenticationException
     */
    @PostMapping("/register")
    public User register(@RequestBody User user) throws AuthenticationException{
        return authService.register(user);
    }

    /**
     * 登录
     * @param loginVo
     * @return
     * @throws AuthenticationException
     */
    @PostMapping("/login")
    public String login(@RequestBody LoginVo loginVo) throws AuthenticationException{
        return authService.login(loginVo.getUsername(),loginVo.getPassword());
    }

}
