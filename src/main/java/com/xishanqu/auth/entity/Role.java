package com.xishanqu.auth.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Author BaoNing 2019/4/1
 */
@Table(name = "sass_role")
@Entity
@Data
public class Role {

    @Id
    @GeneratedValue
    private Integer id;
    private String name;

}
