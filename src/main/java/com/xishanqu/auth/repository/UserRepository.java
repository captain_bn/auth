package com.xishanqu.auth.repository;

import com.xishanqu.auth.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Author BaoNing 2019/4/1
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * 查找用户
     * @param username
     * @return
     */
    User findByUsername(String username);
}
