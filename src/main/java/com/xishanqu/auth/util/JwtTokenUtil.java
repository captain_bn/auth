package com.xishanqu.auth.util;

import com.xishanqu.auth.constant.JwtConstant;
import com.xishanqu.auth.entity.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author BaoNing 2019/4/1
 */
@Component
public class JwtTokenUtil implements Serializable{

    private static final long serialVersionUID = -8816691138570982081L;

    private static final String CLAIM_KEY_USERNAME = "sub";
    private static final String CLAIM_KEY_CREATED = "created";


    /**
     * 获取用户token
     * @param userDetails
     * @return
     */
    public String generateToken(UserDetails userDetails){
        Map<String,Object> claims = new HashMap<>();
        claims.put(CLAIM_KEY_USERNAME, userDetails.getUsername());
        claims.put(CLAIM_KEY_CREATED, new Date());
        return generateToken(claims);
    }

    /**
     * 创建Token
     * @param claims
     * @return
     */
    private String generateToken(Map<String,Object> claims){
        String token = Jwts.builder()
                .setClaims(claims)
                .setExpiration(generateExpirationDate())
                .signWith(SignatureAlgorithm.HS512, JwtConstant.SECRET)
                .compact();
        return token;
    }

    /**
     *  从Token中获取用户声明信息
     * @param token
     * @return
     */
    private Claims getClaimsFromToken(String token){
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(JwtConstant.SECRET)
                    .parseClaimsJws(token)
                    .getBody();
        }catch (Exception e){
            claims = null;
        }
        return claims;
    }

    /**
     * 从Token中获取过期时间
     * @param token
     * @return
     */
    public Date getExpirationDateFromToken(String token){
        Date expiration;
        try {
            final Claims claims = getClaimsFromToken(token);
            expiration = claims.getExpiration();
        }catch (Exception e){
            expiration = null;
        }
        return expiration;
    }

    /**
     * 从Token中获取创建时间
     * @param token
     * @return
     */
    public Date getCreatedDateFromToken(String token){
        Date created;
        try {
            final Claims claims = getClaimsFromToken(token);
            created = new Date((Long) claims.get(CLAIM_KEY_CREATED));
        }catch (Exception e){
            created = null;
        }
        return created;
    }

    /**
     * 从Token中获取用户名
     * @param token
     * @return
     */
    public String getUsernameFromToken(String token){
        String username;
        try{
            final Claims claims = getClaimsFromToken(token);
            username = claims.getSubject();
        }catch (Exception e){
            username = null;
        }
        return username;
    }

    /**
     * 创建过期时间
     * @return
     */
    private Date generateExpirationDate(){
        return new Date(System.currentTimeMillis() + JwtConstant.EXPIRATION_TIME * 1000);
    }

    /**
     * 验证Token是否过期
     * @param token
     * @return
     */
    private Boolean isTokenExpired(String token){
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    /**
     * 验证创建时间和最后登录时间
     * @param created
     * @param lastPassowrdReset
     * @return
     */
    private Boolean isCreatedBeforeLastPasswordReset(Date created, Date lastPassowrdReset){
        return (lastPassowrdReset != null && created.before(lastPassowrdReset));
    }

    /**
     * 验证Token能否被重置
     * @param token
     * @return
     */
    public Boolean canTokenBeRefreshed(String token){
        return !isTokenExpired(token);
    }

    /**
     * 重置Token
     * @param token
     * @return
     */
    public String refreshToken(String token){
        String refreshToken;
        try {
            final Claims claims = getClaimsFromToken(token);
            claims.put(CLAIM_KEY_CREATED, new Date());
            refreshToken = generateToken(claims);
        }catch (Exception e){
            refreshToken = null;
        }
        return refreshToken;
    }

    /**
     * 验证Token
     * @param token
     * @param userDetails
     * @return
     */
    public Boolean validateToken(String token, UserDetails userDetails){
        User user = (User) userDetails;
        final String username = getUsernameFromToken(token);
        return (username.equals(user.getUsername()) && !isTokenExpired(token));
    }

}
