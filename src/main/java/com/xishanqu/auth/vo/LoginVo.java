package com.xishanqu.auth.vo;

import lombok.Data;

/**
 * @Author BaoNing 2019/4/1
 */
@Data
public class LoginVo {

    private String username;
    private String password;

}
